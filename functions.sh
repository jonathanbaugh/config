br() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: br [SUFFIX]...[PREFIX:-feature]'
    echo 'Outputs a predefined branch name PREFIX/$USER/SUFFIX'
    echo '  Example feature: br JIRA-123        (outputs feature/jbaugh/JIRA-123)'
    echo '  Example bugfix:  br JIRA-123 bugfix (outputs bugfix/jbaugh/JIRA-123)'
    echo '  Example hotfix:  br JIRA-123 hotfix (outputs hotfix/jbaugh/JIRA-123)'
    echo 'See also commit, nt'
    return
  fi
  echo ${2:-feature}/${USER}/$1
}

co() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: co [SUFFIX]...[PREFIX:-feature]'
    echo 'Checks out the specified ticket branch if found, otherwise, will checkout'
    echo 'a matching branch without prefix'
    echo 'See also br, nt, ticket'
    return
  fi
  checkout=$(git checkout `br $1 $2` 2> /dev/null)
  checkout_exit=$?
  if [[ $checkout_exit = 0 ]]; then
    echo "$checkout"
  else
    checkout_branch=$(git checkout $1 2> /dev/null)
    checkout_exit=$?
    if [[ $checkout_exit = 0 ]]; then
      echo "$checkout_branch"
    else
      echo "No matching branch found for $1"
    fi
  fi
}

commit() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: commit MESSAGE...[git OPTIONS]'
    echo 'Creates a new commit with a properly formatted message prefix'
    echo '  Example: commit "Add feature" (runs git commit -m "[JIRA-123] Add feature")'
    echo '  Example: commit "Add feature" -n (runs git commit -m "[JIRA-123] Add feature" -n)'
    echo 'See also br, nt, ticket'
    return
  fi
  message=$1
  shift
  git commit -m "`ticket_prefix` $message" $@
}

gb() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: gb'
    echo 'Output current branch name'
    echo 'See also br, nt, ticket'
    return
  fi
  git rev-parse --abbrev-ref HEAD
}

nt() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: nt [SUFFIX]...[PREFIX:-feature]'
    echo 'Creates a new git branch for a New Ticket PREFIX/$USER/SUFFIX'
    echo '  Example feature: nt JIRA-123        (creates feature/jbaugh/JIRA-123)'
    echo '  Example bugfix:  nt JIRA-123 bugfix (creates bugfix/jbaugh/JIRA-123)'
    echo '  Example hotfix:  nt JIRA-123 hotfix (creates hotfix/jbaugh/JIRA-123)'
    echo 'See also br'
    return
  fi
  git fetch origin ${GIT_MAIN_BRANCH:-master} --quiet
  git checkout -b `br $1 $2` "origin/${GIT_MAIN_BRANCH:-master}"
}

ticket() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: ticket'
    echo 'Outputs the ticket number based on the current git branch name'
    echo '  Example branch feature/jbaugh/JIRA-123:          Outputs "JIRA-123"'
    echo '  Example branch feature/jbaugh/JIRA-123-part-two: Outputs "JIRA-123"'
    echo '  Example branch master:                           Outputs "X" (or DEFAULT_TICKET)'
    echo 'See also br, commit, nt'
    return
  fi
  _ticket=$(git rev-parse --abbrev-ref HEAD | cut -d'/' -f3 | grep --only-matching --color=none --extended-regexp '([A-Z]+-)?[0-9]+' \
    || echo "${DEFAULT_TICKET:-X}")
  [[ "$_ticket" =~ '[A-Z]+-[0-9]+' ]] && echo $_ticket || echo "#${_ticket}"
}

ticket_prefix() {
  _ticket=$(ticket)
  [[ "${_ticket}" =~ '#.*' ]] && echo "[${_ticket}]" || echo "[`ticket`]"
}

master() {
  DEFAULT_BRANCH=$([ -f .git/refs/heads/master ] && echo master || echo main)
  git checkout "${GIT_MAIN_BRANCH:-$DEFAULT_BRANCH}"
}

push() {
  git push origin `gb`
}

dcf() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: dcf'
    echo 'Convenience method for working with verbose docker-compose file names'
    echo '  Example dcf test up -> Runs docker-compose -f docker-compose.test.yaml up'
    return
  fi
  docker-compose -f docker-compose.$1.yaml ${@[@]:2}
}

detour() {
  if [[ "$1" == "--help" ]]; then
    echo 'Usage: detour TO [git checkout args]; detour # from'
    echo 'Quickly save a work in progress commit and switch to another branch'
    echo 'When switching back, revert the commit.'
    echo '  Example:'
    echo '    nt TICK-1 # Create a new ticket branch'
    echo '    detour `br TICK-2` # Switch to feature/jbaugh/TICK-2'
    echo '    detour # Switch back to TICK-1'
    return
  fi
  _DETOUR_FROM=$(git rev-parse --abbrev-ref HEAD)
  _DETOUR_TO=$1

  if [ -z "$_DETOUR_TO" ]; then
    if [ -z "$DETOUR_FROM" ]; then
      echo "No detour found"
      return
    fi
    git checkout $DETOUR_FROM \
    && git reset --soft HEAD~
    echo "Switch back to $_DETOUR_FROM"
    unset DETOUR_FROM
  else
    echo "Switch to $_DETOUR_TO"
    git add --all \
    && git commit \
      --no-gpg-sign --no-verify \
      --allow-empty \
      --message "Detour to $_DETOUR_TO" \
    && git checkout ${@[@]:2} $_DETOUR_TO
    export DETOUR_FROM=$_DETOUR_FROM
  fi
}

# Set the WORKING_DIR when initialized
_WORKING_DIR=$(git rev-parse --show-toplevel 2>/dev/null || pwd)
wd() {
  cd "${_WORKING_DIR}"
}
