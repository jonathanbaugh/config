DIR=$(dirname $0)
source "${DIR}/alias.sh"
source "${DIR}/functions.sh"
source "${DIR}/paths.sh"

if [[ -f .env ]]; then
  source .env
fi
