alias pbcopy='xclip -selection clipboard'
alias pbpaste='xclip -selection clipboard -o'

alias dc='docker compose'
alias k='kubectl'

alias add='git add -A'
alias st='git status'
alias tf='terraform'
alias tg='terragrunt'
